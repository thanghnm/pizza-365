const express = require("express");

const router = express.Router();

const {
    getAllVouchersMiddleware,
    createVoucherMiddleware,
    getVoucherByIDMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
} = require("../middlewares/voucherMiddleware");
const {  createVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getAllVoucher, getVoucherByMaVoucher } = require("../controllers/voucherController");
router.get("/all/", getAllVouchersMiddleware, getAllVoucher);

router.get("/:maVoucher", getAllVouchersMiddleware, getVoucherByMaVoucher);

router.post("/", createVoucherMiddleware, createVoucher)

router.get("/id/:voucherId", getVoucherByIDMiddleware, getVoucherById)

router.put("/:voucherId", updateVoucherMiddleware, updateVoucherById)

router.delete("/:voucherId", deleteVoucherMiddleware, deleteVoucherById)

module.exports = router;

