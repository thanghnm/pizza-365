const express = require("express");

const router = express.Router();

const { createUser, getAllUser, updateUserById, deleteUserById, getUserById } = require("../controllers/userController");

router.get("/", getAllUser);

router.post("/", createUser)

router.get("/:userId", getUserById)

router.put("/:userId", updateUserById)

router.delete("/:userId", deleteUserById)

module.exports = router;

