const express = require("express");

const router = express.Router();

const {
    getAllOrdersMiddleware,
    createOrderMiddleware,
    getOrderByIDMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
} = require("../middlewares/orderMiddleware");
const { getAllOrder, createOrder, getOrderById, updateOrderById, deleteOrderById, createOrderToUser } = require("../controllers/orderController");

router.get("/", getAllOrdersMiddleware, getAllOrder);

router.post("/", createOrderMiddleware, createOrderToUser)
router.post("/all", createOrderMiddleware, createOrder)
router.get("/:orderId", getOrderByIDMiddleware, getOrderById)

router.put("/:orderId", updateOrderMiddleware, updateOrderById)

router.delete("/:orderId", deleteOrderMiddleware, deleteOrderById)


module.exports = router;

