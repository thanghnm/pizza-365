const express = require("express");

const router = express.Router();

const {
    getAllDrinkMiddleware,
    createDrinkMiddleware,
    getDrinkByIDMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
} = require("../middlewares/drinkMiddleware");
const { getDrinkList, createDrink, getDrinkById, updateDrinkById, deleteDrinkById } = require("../controllers/drinkController");

router.get("/", getAllDrinkMiddleware, getDrinkList);

router.post("/", createDrinkMiddleware, createDrink)

router.get("/:drinkId",getDrinkByIDMiddleware, getDrinkById)

router.put("/:drinkId", updateDrinkMiddleware, updateDrinkById)

router.delete("/:drinkId", deleteDrinkMiddleware,deleteDrinkById)

module.exports = router;

