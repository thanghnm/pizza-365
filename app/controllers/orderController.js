//Import Model
const orderModel = require("../model/orderModel")
const mongoose = require("mongoose")
const userModel = require("../model/userModel")
const voucherModel = require("../model/voucherModel")
const drinkModel = require("../model/drinkModel")

// Hàm tạo Drink
const createOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { orderCode, pizzaSize, pizzaType, status, userId } = req.body
    // B2 Kiểm tra dữ liệu
    if (!orderCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required"
        })
    }
    if (!pizzaSize) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required"
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required"
        })
    }
    if (!status) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }
    // B3 Xử lý 
    let newOrder = {
        orderCode,
        pizzaSize,
        pizzaType,
        status,
    }
    try {
        const result = await orderModel.create(newOrder);
        const updateOrderOfUser = await userModel.findByIdAndUpdate(userId, {
            $push: { order: result._id }
        })
        return res.status(201).json({
            status: "create order successfully",
            result,
            // updateOrderOfUser
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

    /* Chasing
    {
        {
            "_id": "651c240dd276f7fb281a40c5",
            "fullName": "ABC",
            "email": "qwer",
            "address": "xyz",
            "phone": "012312313",
            "order": [
                "651c240dd276f7fb281a40c4"
            ],
            "__v": 0
        }
    */
}
// Hàm Lấy dữ liệu tất cả Drink
const getAllOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await orderModel.find().populate("drink");
    return res.status(200).json({
        result
    });

}
// Hàm lấy dữ liệu Drink bằng Id
const getOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await orderModel.findById(orderId);
    return res.status(200).json({
        result
    });
    /* Chasing
{
"result": {
            "_id": "651c240dd276f7fb281a40c5",
            "fullName": "ABC",
            "email": "qwer",
            "address": "xyz",
            "phone": "012312313",
            "order": [
                "651c240dd276f7fb281a40c4"
            ],
            "__v": 0
        }
}
*/
}
// Hàm Update Drink bằng Id
const updateOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var orderId = req.params.orderId
    const { orderCode, pizzaSize, pizzaType, status, userId } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedOrder = {
        orderCode,
        pizzaSize,
        pizzaType,
        status,
    }
    const result = await orderModel.findByIdAndUpdate(orderId, updatedOrder);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });
    /* Chasing
   {
            "_id": "651c240dd276f7fb281a40c5",
            "fullName": "ABCD",
            "email": "qwere",
            "address": "xyza",
            "phone": "012312314",
            "order": [
                "651c240dd276f7fb281a40c4"
            ],
            "__v": 0
        }
}       
    */
}
// Hàm xóa Drink bằng Id
const deleteOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.body
    const orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await orderModel.findByIdAndDelete(orderId);
    return res.status(204).json({
        status: "Delete Order successfully"
    });
}
// Hàm tạo order cho user
const createOrderToUser = async (req, res) => {
    var { email, kichCo, loaiPizza, diaChi, soDienThoai, idVourcher, duongKinh, thanhTien, loiNhan,
        salad, soLuongNuoc, suon, idLoaiNuocUong, hoTen } = req.body
    if (!email) {
        return res.status(400).json({
            status: "Bad request"
        })
    }
    // Tạo mã đơn hàng ngẫu nhiên
    const getRandomCode = getRandomOrderCode()
    const condition = {}
    //Tìm user trong database theo email
    if (email) {
        condition.email = email
    }
    const findUser = await userModel.findOne(condition)
    //Tìm voucher trong database theo mã voucher

    const conditionVoucher = {}
    conditionVoucher.maVoucher = idVourcher
    const findVoucher = await voucherModel.findOne(conditionVoucher)
    //Tìm nước uống trong database theo mã nước uống

    const conditionDrink = {}
    conditionDrink.maNuocUong = idLoaiNuocUong
    const findDrink = await drinkModel.findOne(conditionDrink)

    if (findDrink == null && findVoucher !== null) {
        var newOrder = {
            orderCode: getRandomCode,
            pizzaSize: kichCo,
            pizzaType: loaiPizza,
            status: "open"
        }
    }
    if (findVoucher == null && findDrink !== null) {
        var newOrder = {
            orderCode: getRandomCode,
            pizzaSize: kichCo,
            pizzaType: loaiPizza,
            drink: findDrink._id,
            status: "open"
        }
    }

    if (findDrink == null && findVoucher !== null) {
        var newOrder = {
            orderCode: getRandomCode,
            pizzaSize: kichCo,
            pizzaType: loaiPizza,
            voucher: findVoucher._id,
            status: "open"
        }
    }

    if (findDrink !== null && findVoucher !== null) {
        console.log(findVoucher)
        var newOrder = {
            orderCode: getRandomCode,
            pizzaSize: kichCo,
            pizzaType: loaiPizza,
            voucher: findVoucher._id,
            drink: findDrink._id,
            status: "open"
        }
    }
    try {
        //Tạo order
        var createOrder = await orderModel.create(newOrder)

    } catch (error) {
        res.status(400).json({
            message: error.message
        })
    }

    // Nếu chưa có user
    if (findUser == null) {
        const newUser = {
            fullName: hoTen,
            email,
            address: diaChi,
            phone: soDienThoai
        }
        // Kiểm tra xem số điện thoại có bị trùng không
        const conditionPhone = {}
        conditionPhone.phone = soDienThoai
        const findPhone = await userModel.findOne(conditionPhone)
        if (findPhone) {
            return res.status(400).json({
                message: "Số điện thoại đã được đăng ký"
            })
        }
        const createUser = await userModel.create(newUser)
        // Thêm order vào user
        const updateOrderOfUser = await userModel.findByIdAndUpdate(createUser._id, {
            $push: { order: createOrder._id }
        })
        // Nếu chưa có user và không tìm thấy voucher 
        if (findVoucher == null) {
            try {
                return res.status(201).json({
                    id: updateOrderOfUser._id,
                    hoVaTen: updateOrderOfUser.fullName,
                    email,
                    diaChi: updateOrderOfUser.address,
                    soDienThoai: updateOrderOfUser.phone,
                    kichCo: createOrder.pizzaSize,
                    loaiPizza: createOrder.pizzaType,
                    orderCode: createOrder.orderCode,
                    duongKinh,
                    thanhTien,
                    loiNhan,
                    salad,
                    soLuongNuoc,
                    suon,
                    trangThai: createOrder.status
                })
            } catch (error) {
                return res.json({
                    message: error.message
                })
            }
        }
        // Nếu chưa có user và tìm thấy voucher
        try {
            return res.status(201).json({
                id: updateOrderOfUser._id,
                hoVaTen: updateOrderOfUser.fullName,
                email,
                diaChi: updateOrderOfUser.address,
                soDienThoai: updateOrderOfUser.phone,
                kichCo: createOrder.pizzaSize,
                loaiPizza: createOrder.pizzaType,
                maVoucher: findVoucher.maVoucher,
                idVourcher: findVoucher._id,
                orderCode: createOrder.orderCode,
                duongKinh,
                giamGia: vGiamGiaVoucher,
                thanhTien,
                loiNhan,
                salad,
                soLuongNuoc,
                suon,
                trangThai: createOrder.status
            })
        } catch (error) {
            return res.json({
                message: error.message
            })
        }
    }
    // Nếu đã có user

    else {
        const updateOrderOfUser = await userModel.findByIdAndUpdate(findUser._id, {
            $push: { order: createOrder._id }
        })
        // Nếu đã có user và không tìm thấy voucher
        if (findVoucher == null) {
            try {
                return res.status(201).json({
                    id: updateOrderOfUser._id,
                    hoVaTen: updateOrderOfUser.fullName,
                    email,
                    diaChi: updateOrderOfUser.address,
                    soDienThoai: updateOrderOfUser.phone,
                    kichCo: createOrder.pizzaSize,
                    loaiPizza: createOrder.pizzaType,
                    orderCode: createOrder.orderCode,
                    duongKinh,
                    thanhTien,
                    loiNhan,
                    salad,
                    soLuongNuoc,
                    suon,
                    trangThai: createOrder.status
                })
            } catch (error) {
                return res.json({
                    message: error.message
                })
            }
        }
        //Nếu đã có user và tìm thấy voucher
        else {
            try {
                return res.status(201).json({
                    id: updateOrderOfUser._id,
                    hoVaTen: updateOrderOfUser.fullName,
                    email,
                    diaChi: updateOrderOfUser.address,
                    soDienThoai: updateOrderOfUser.phone,
                    kichCo: createOrder.pizzaSize,
                    loaiPizza: createOrder.pizzaType,
                    maVoucher: findVoucher.maVoucher,
                    idVourcher: findVoucher._id,
                    orderCode: createOrder.orderCode,
                    duongKinh,
                    giamGia: findVoucher.phanTramGiamGia,
                    thanhTien,
                    loiNhan,
                    salad,
                    soLuongNuoc,
                    suon,
                    trangThai: createOrder.status
                })
            } catch (error) {
                return res.json({
                    message: error.message
                })
            }
        }
    }


}
// Hàm tạo mã đơn hàng ngẫu nhiên
function getRandomOrderCode() {
    let code = ""
    const character = "ABCDEFGHIJKMLOPRQVUSZWYTabcdefghijklmoprqvuzxwyt"
    for (var bI = 0; bI < 10; bI++) {
        code += character.charAt(Math.floor(Math.random() * 10))
    }
    return code
}

// Export
module.exports = {
    createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById, createOrderToUser
}