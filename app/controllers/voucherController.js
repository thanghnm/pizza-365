//Import Model
const voucherModel = require("../model/voucherModel")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createVoucher = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body
    // B2 Kiểm tra dữ liệu
    if (!maVoucher) {
        return res.status(400).json({
            status: "Bad request",
            message: "maVoucher is required"
        })
    }
    if (!phanTramGiamGia) {
        return res.status(400).json({
            status: "Bad request",
            message: "phanTramGiamGia is required"
        })
    }
    // B3 Xử lý 

    try {
        let newVoucher = {
            _id: new mongoose.Types.ObjectId(),
            maVoucher,
            phanTramGiamGia,
            ghiChu
        }
        const result = await voucherModel.create(newVoucher);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

    /* Chasing
    {
        "result": {
            "_id": "651bdd6e024455e4476629cb",
            "maVoucher": "VC20",
            "phanTramGiamGia": 20,
            "__v": 0
        }
    }       
    */
}
// Hàm Lấy dữ liệu tất cả Voucher
const getVoucherByMaVoucher = async (req, res) => {
    // B1 Thu thập dữ liệu\
    const maVoucher = req.params.maVoucher
    const condition = {}

    if (maVoucher)
        condition.maVoucher = maVoucher
    if (!maVoucher) {
        res.status(400).json({
            message: "Mã giảm giá không tồn tại"
        })
    }
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    try {
        const result = await voucherModel.findOne(condition);
        if (result) {
            return res.status(200).json({
                result
            });
        }

    } catch (error) {
        return res.status(500).josn({
            message:
                message.error
        })
    }


}
// Hàm Lấy dữ liệu tất cả Voucher
const getAllVoucher = async (req, res) => {
    // B1 Thu thập dữ liệu\
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    try {
        const result = await voucherModel.find();
        if (result) {
            return res.status(200).json({
                result
            });
        }
    } catch (error) {
        return res.status(500).josn({
            message:
                message.error
        })
    }
    /* Chasing
{
    "result": [
        {
            "_id": "651bdd6e024455e4476629cb",
            "maVoucher": "VC20",
            "phanTramGiamGia": 20,
            "__v": 0
        }
    ]
}
*/
}
// Hàm lấy dữ liệu Voucher bằng Id
const getVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await voucherModel.findById(voucherId);
    return res.status(200).json({
        result
    });
    /* Chasing
        {
            "result": {
                "_id": "651bdd6e024455e4476629cb",
                "maVoucher": "VC20",
                "phanTramGiamGia": 20,
                "__v": 0
            }
        }
*/
}
// Hàm Update Voucher bằng Id
const updateVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedVoucher = {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    }
    const result = await voucherModel.findByIdAndUpdate(voucherId, updatedVoucher);
    return res.status(200).json({
        status: "Update Voucher successfully",
        result
    });
    /* Chasing
   {
    "status": "Update Voucher successfully",
    "result": {
        "_id": "651bdd6e024455e4476629cb",
        "maVoucher": "VC30",
        "phanTramGiamGia": 30,
        "__v": 0
    }
}
}       
    */
}
// Hàm xóa Voucher bằng Id
const deleteVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await voucherModel.findByIdAndDelete(voucherId);
    return res.status(204).json({
        status: "Delete Voucher successfully"
    });
}
// Export
module.exports = { createVoucher, getVoucherByMaVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getAllVoucher }