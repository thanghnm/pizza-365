//Import Model
const drinkModel = require("../model/drinkModel")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createDrink = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { maNuocUong, tenNuocUong, donGia } = req.body
    // B2 Kiểm tra dữ liệu
    if (!maNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "maNuocUong is required"
        })
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "tenNuocUong is required"
        })
    }
    if (!donGia || donGia <= 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "Đơn giá không hợp lệ"
        })
    }
    // B3 Xử lý 
    let newDrink = {
        _id: new mongoose.Types.ObjectId(),
        maNuocUong,
        tenNuocUong,
        donGia
    }
    try {
        const result = await drinkModel.create(newDrink);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

    /* Chasing
    {
        "test": {
            "_id": "651bc8c039db62c35c9595fe",
            "maNuocUong": "xuz",
            "tenNuocUong": "NuocNgot",
            "donGia": 10000,
            "__v": 0
        }
    */
}
// Hàm Lấy dữ liệu tất cả Drink
const getDrinkList = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await drinkModel.find();
    return res.status(200).json({
        result
    });

}
// Hàm lấy dữ liệu Drink bằng Id
const getDrinkById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var drinkId = req.params.drinkId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    const result = await drinkModel.findById(drinkId);
    return res.status(200).json({
        result
    });
    /* Chasing
{
"result": {
            "_id": "651bc9ffd901c0c16c542dfd",
            "maNuocUong": "nmn",
            "tenNuocUong": "Trada",
            "donGia": 10000,
            "__v": 0
        }
}
*/
}
// Hàm Update Drink bằng Id
const updateDrinkById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var drinkId = req.params.drinkId
    const { maNuocUong, tenNuocUong, donGia } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedDrink = {
        maNuocUong,
        tenNuocUong,
        donGia
    }
    const result = await drinkModel.findByIdAndUpdate(drinkId, updatedDrink);
    return res.status(200).json({
        status: "Update Drink successfully",
        result
    });
    /* Chasing
   {
    "status": "Update Drink successfully",
    "result": {
        "_id": "651bc9ffd901c0c16c542dfd",
        "maNuocUong": "opo",
        "tenNuocUong": "Trada",
        "donGia": 10000,
        "__v": 0
    }
}       
    */
}
// Hàm xóa Drink bằng Id
const deleteDrinkById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var drinkId = req.params.drinkId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await drinkModel.findByIdAndDelete(drinkId);
    return res.status(204).json({
        message: "Delete Drink successfully"
    });
}
// Export
module.exports = { createDrink, getDrinkList, getDrinkById, updateDrinkById, deleteDrinkById }