//Import Model
const userModel = require("../model/userModel")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { fullName, email, address, phone } = req.body
    // B2 Kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }
    // B3 Xử lý 
    let newUser = {
        fullName,
        email,
        address,
        phone
    }
    try {
        const result = await userModel.create(newUser);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

    /* Chasing
    {
        "result": {
            "_id": "651c240dd276f7fb281a40c5",
            "fullName": "ABC",
            "email": "qwer",
            "address": "xyz",
            "phone": "012312313",
            "order": [
                "651c240dd276f7fb281a40c4"
            ],
            "__v": 0
        }
    */
}
// Hàm Lấy dữ liệu tất cả Drink
const getAllUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.find().populate({path:"order",populate:{path:"drink"}});
    return res.status(200).json({
        result
    });

}
// Hàm lấy dữ liệu User bằng Id
const getUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await userModel.findById(userId);
    return res.status(200).json({
        result
    });

}

// Hàm Update Drink bằng Id
const updateUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    const { fullName, email, address, phone } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid"
        })
    }
    try {

        // B3 Xử lý 
        let updatedUser = {
            fullName,
            email,
            address,
            phone
        }
        const result = await userModel.findByIdAndUpdate(userId, updatedUser);
        return res.status(200).json({
            status: "Update User successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            error
        })
    }

    /* Chasing
   {
    "status": "Update User successfully",
    "result": {
            "_id": "651c240dd276f7fb281a40c5",
            "fullName": "ABCD",
            "email": "qwere",
            "address": "xyzz",
            "phone": "012312315",
            "order": [
                "651c240dd276f7fb281a40c4"
            ],
            "__v": 0
        }
}       
    */
}
// Hàm xóa Drink bằng Id
const deleteUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.findByIdAndDelete(userId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = {
    createUser, getAllUser, getUserById, updateUserById, deleteUserById, 
}