const getAllOrdersMiddleware = (req, res, next) => {
    console.log("GET all orders middleware");

    next();
}

const createOrderMiddleware = (req, res, next) => {
    console.log("POST order middleware");

    next();
}

const getOrderByIDMiddleware = (req, res, next) => {
    console.log("GET order by id middleware");

    next();
}

const updateOrderMiddleware = (req, res, next) => {
    console.log("PUT order middleware");

    next();
}

const deleteOrderMiddleware = (req, res, next) => {
    console.log("DELETE order middleware");

    next();
}

module.exports = {
    getAllOrdersMiddleware,
    createOrderMiddleware,
    getOrderByIDMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}
