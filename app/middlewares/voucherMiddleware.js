const getAllVouchersMiddleware = (req, res, next) => {
    console.log("GET all vouchers middleware");

    next();
}

const createVoucherMiddleware = (req, res, next) => {
    console.log("POST voucher middleware");

    next();
}

const getVoucherByIDMiddleware = (req, res, next) => {
    console.log("GET voucher by id middleware");

    next();
}

const updateVoucherMiddleware = (req, res, next) => {
    console.log("PUT voucher middleware");

    next();
}

const deleteVoucherMiddleware = (req, res, next) => {
    console.log("DELETE voucher middleware");

    next();
}

module.exports = {
    getAllVouchersMiddleware,
    createVoucherMiddleware,
    getVoucherByIDMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
}
