const getAllDrinkMiddleware = (req, res, next) => {
    console.log("GET all drinks middleware");

    next();
}

const createDrinkMiddleware = (req, res, next) => {
    console.log("POST drink middleware");

    next();
}

const getDrinkByIDMiddleware = (req, res, next) => {
    var drinkId = req.params.drinkId
    console.log("GET drink by id ",drinkId);
    next();
}

const updateDrinkMiddleware = (req, res, next) => {
    var drinkId = req.params.drinkId
    console.log("PUT drink by Id",drinkId);
    next();
}

const deleteDrinkMiddleware = (req, res, next) => {
    console.log("DELETE drink middleware");

    next();
}

module.exports = {
    getAllDrinkMiddleware,
    createDrinkMiddleware,
    getDrinkByIDMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
}
