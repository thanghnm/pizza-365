//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
	maNuocUong:{
        type:String,
        required:true,
        unique:true

    },
    tenNuocUong:{
        type:String,
        required:true,

    },
    donGia:{
        type:Number,
        required:true
    }
    

})
// Biên dịch Schema
module.exports = mongoose.model("Drink",drinkSchema)
