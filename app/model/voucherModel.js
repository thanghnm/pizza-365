//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher:{
        type:String,
        required:true,
        unique:true

    },
    phanTramGiamGia:{
        type:Number,
        required:true,

    },
    ghiChu:{
        type:String,
        required:false
    }
})
// Biên dịch Schema
module.exports = mongoose.model("voucher",voucherSchema)
