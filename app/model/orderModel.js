//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const orderSchema = new Schema({
    
    orderCode:{
        type:String,
        required:true,
        unique:true
    },
    pizzaSize:{
        type:String,
        required:true,

    },
    pizzaType:{
        type:String,
        required:true
    },
    voucher:{
        type: Schema.Types.ObjectId,
        ref:"Voucher"

    },
    drink:{
        type: Schema.Types.ObjectId,
        ref:"Drink"

    },
    status:{
        type:String,
        required:true
    }
})
// Biên dịch Schema
module.exports = mongoose.model("Order",orderSchema)