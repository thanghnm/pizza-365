//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const userSchema = new Schema({
    
    fullName:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true,
        unique:true

    },
    address:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true,
        unique:true

    },
    order:
        [{ type: Schema.Types.ObjectId, ref: 'Order' }]
    
})
// Biên dịch Schema
module.exports = mongoose.model("User",userSchema)