
<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>
<!-- ABOUT THE PROJECT -->
## ABOUT THE PROJECT
PROJECT PIZZA365
This project was created with the purpose of providing a website to order pizza online
### Node.js
Node.JS is the core of our template. If you don't know what Node.js is or don't know how to use it, we strongly recommend checking the Node.js before start doing anything with Pizza365.
## Installation
**A.Installing Prerequisites**
<br />
Download and install at least LTS or the latest version of [Node.js](https://nodejs.org/) from its web site.

**B.Installing Pizza365 **
<br />
Open your favorite console application (Terminal, Command Prompt etc.), navigate into your work folder, run the following command and wait for it to finish:
<br />

`npm install`

<br />
This command will install all the required Node.js modules into the node_modules directory inside your work folder.

<br />
And now, you are ready to run the Pizza365 for the first time.
### Development
While still in your work folder, run the following command in the console application:
<br />

`npm start`

<br />
And that's it. you are starting the Pizza365.
<br/>
You can check out your console application to get further information about the server. By default, it will run on [http://localhost:8000](http://localhost:8000) but it might change depending on your setup
## Sources and Credits
- bootstrap 4.x
- fslightbox-react
- express
- mongoose
- chai
- chai-http
- supertest
- mocha
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
## Learn More

To learn React, check out the [React documentation](https://reactjs.org/).

