const request = require("supertest");
const orderModel = require("../app/model/orderModel");
const app = require("../index");
const mongoose = require('mongoose');
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
describe("MongoDB Connection", () => {
    it("should be able to connect to MongoDB", (done) => {
        const connectionState = mongoose.connection.readyState;
        connectionState.should.eql(1); // 1 - đang kết nối
        done();

        // // 1 trong 2 cách
        // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
        //     .get("/")
        //     .end(function (err, res) {
        //         res.status.should.eql(200);
        //         done();
        //     });
    });

});

describe("Test CRUD Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            const condition = {}
            condition.orderCode = "code"
            await orderModel.findOneAndDelete(condition);
        });

        it("should create a new user", async () => {
            const res = await chai
                .request(app)
                .post("/api/devcamp-pizza365/orders/all")
                .send({
                    orderCode: "code",
                    pizzaSize: "abc",
                    pizzaType: "abc",
                    status: "open"
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("orderCode").eql("code");
        });
    });

    // Test request Get /
    describe("/GET/ - Get all", () => {
        it("should return all order must be array", (done) => {
            chai.request(app)
                .get("/api/devcamp-pizza365/orders/")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.result.should.be.a("array");
                    done();
                });
        });
    });
    // Test request Get by Id
    describe("/GET/:id - Get one", () => {
        after(async () => {
            const condition = {}
            condition.orderCode = "code"
            await orderModel.findOneAndDelete(condition);
        });

        it("should retrieve a order by its ID", async () => {
            const order = await orderModel.create({
                _id: new mongoose.Types.ObjectId,
                orderCode: "code",
                pizzaSize: "abc",
                pizzaType: "abc",
                status: "open"
            });

            const res = await chai
                .request(app)
                .get("/api/devcamp-pizza365/orders/" + order._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have
                .property("_id")
                .eql(order._id.toString());
        });
    });


    // Test request update by Id
    describe("PUT /:Id", () => {
        after(async () => {
            const condition = {}
            condition.orderCode = "code updated"
            await orderModel.findOneAndDelete(condition);
        });

        it("should update a user by id", async () => {
            const updateData = await orderModel.create({
                _id: new mongoose.Types.ObjectId,
                orderCode: "code",
                pizzaSize: "abc",
                pizzaType: "abc",
                status: "open"
            });

            const res = await chai
                .request(app)
                .put("/api/devcamp-pizza365/orders/" + updateData._id)
                .send({
                    orderCode: "code updated",
                    pizzaSize: "abc",
                    pizzaType: "abc",
                    status: "open"
                });
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("status");
            res.body.result.should.have.property("status")
        });
    });
    describe("/DELETE/:id - Delete one", () => {
        it("should delete  by id", async () => {
            let order = await orderModel.create({
                _id: new mongoose.Types.ObjectId,
                orderCode: "code updated",
                pizzaSize: "abc",
                pizzaType: "abc",
                status: "open"
            });

            const res = await chai
                .request(app)
                .delete("/api/devcamp-pizza365/orders/" + order._id);

            res.should.have.status(204);
            res.body.should.be.a("object");

        });
    });
})
