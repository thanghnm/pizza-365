const request = require("supertest");
const drinkModel = require("../app/model/drinkModel");
const app = require("../index");
const mongoose = require('mongoose');
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
describe("MongoDB Connection", () => {
    it("should be able to connect to MongoDB", (done) => {
        const connectionState = mongoose.connection.readyState;
        connectionState.should.eql(1); // 1 - đang kết nối
        done();
    });

});

describe("Test CRUD Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            const condition = {}
            condition.maNuocUong = "code"
            await drinkModel.findOneAndDelete(condition);
        });

        it("should create a new drink", async () => {
            const res = await chai
                .request(app)
                .post("/api/devcamp-pizza365/drinks/")
                .send({
                    maNuocUong: "code",
                    tenNuocUong: "name",
                    donGia: 1
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("tenNuocUong").eql("name");
        });
    });

    // Test request Get /
    describe("/GET/ - Get all", () => {
        it("should return all drink must be array", (done) => {
            chai.request(app)
                .get("/api/devcamp-pizza365/drinks/")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.result.should.be.a("array");
                    done();
                });
        });
    });
    // Test request Get by Id
    describe("/GET/:id - Get one", () => {
        after(async () => {
            const condition = {}
            condition.maNuocUong = "code"
            await drinkModel.findOneAndDelete(condition);
        });

        it("should retrieve a productType by its ID", async () => {
            const drink = await drinkModel.create({
                _id: new mongoose.Types.ObjectId,
                maNuocUong: "code",
                tenNuocUong: "name",
                donGia: 1
            });

            const res = await chai
                .request(app)
                .get("/api/devcamp-pizza365/drinks/" + drink._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have
                .property("_id")
                .eql(drink._id.toString());
        });
    });


    // Test request update by Id
    describe("PUT /:Id", () => {
        after(async () => {
            const condition = {}
            condition.maNuocUong = "code updated"
            await drinkModel.findOneAndDelete(condition);
        });

        it("should update a drink by id", async () => {
            const updateData = await drinkModel.create({
                _id: new mongoose.Types.ObjectId,
                maNuocUong: "code",
                tenNuocUong: "name",
                donGia: 1
            });

            const res = await chai
                .request(app)
                .put("/api/devcamp-pizza365/drinks/" + updateData._id)
                .send({
                    maNuocUong: "code updated",
                    tenNuocUong: "name",
                    donGia: 1
                });
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("tenNuocUong")
        });
    });
    describe("/DELETE/:id - Delete one", () => {
        it("should delete a produceType by id", async () => {
            let drink = await drinkModel.create({
                _id: new mongoose.Types.ObjectId,
                maNuocUong: "code",
                tenNuocUong: "name",
                donGia: 1
            });

            const res = await chai
                .request(app)
                .delete("/api/devcamp-pizza365/drinks/" + drink._id);

            res.should.have.status(204);
            res.body.should.be.a("object");

        });
    });
})

