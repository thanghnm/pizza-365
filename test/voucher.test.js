const request = require("supertest");
const voucherModel = require("../app/model/voucherModel");
const app = require("../index");
const mongoose = require('mongoose');
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
describe("MongoDB Connection", () => {
    it("should be able to connect to MongoDB", (done) => {
        const connectionState = mongoose.connection.readyState;
        connectionState.should.eql(1); // 1 - đang kết nối
        done();

        // // 1 trong 2 cách
        // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
        //     .get("/")
        //     .end(function (err, res) {
        //         res.status.should.eql(200);
        //         done();
        //     });
    });

});

describe("Test CRUD Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            const condition = {}
            condition.maVoucher = "code"
            await voucherModel.findOneAndDelete(condition);
        });

        it("should create a new voucher", async () => {
            const res = await chai
                .request(app)
                .post("/api/devcamp-pizza365/vouchers/")
                .send({
                    maVoucher: "code",
                    phanTramGiamGia: 1,
                    ghiChu: "abc"
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("ghiChu").eql("abc");
        });
    });

    // Test request Get /
    describe("/GET/ - Get all", () => {
        it("should return all voucher must be array", (done) => {
            chai.request(app)
                .get("/api/devcamp-pizza365/vouchers/all/")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.result.should.be.a("array");
                    done();
                });
        });
    });
    // Test request Get by Id
    describe("/GET/:id - Get one", () => {
        after(async () => {
            const condition = {}
            condition.maVoucher = "code"
            await voucherModel.findOneAndDelete(condition);
        });

        it("should retrieve a productType by its ID", async () => {
            const voucher = await voucherModel.create({
                _id: new mongoose.Types.ObjectId,
                maVoucher: "code",
                phanTramGiamGia: 1,
                ghiChu: "abc"
            });

            const res = await chai
                .request(app)
                .get("/api/devcamp-pizza365/vouchers/id/" + voucher._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have
                .property("_id")
                .eql(voucher._id.toString());
        });
    });


    // Test request update by Id
    describe("PUT /:Id", () => {
        after(async () => {
            const condition = {}
            condition.maVoucher = "code updated"
            await voucherModel.findOneAndDelete(condition);
        });

        it("should update a drink by id", async () => {
            const updateData = await voucherModel.create({
                _id: new mongoose.Types.ObjectId,
                maVoucher: "code",
                phanTramGiamGia: 1,
                ghiChu: "abc"
            });

            const res = await chai
                .request(app)
                .put("/api/devcamp-pizza365/vouchers/" + updateData._id)
                .send({
                    maVoucher: "code updated",
                    phanTramGiamGia: 1,
                    ghiChu: "abc"
                });
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("status");
            console.log(res.body)
        });
    });
    describe("/DELETE/:id - Delete one", () => {
        it("should delete  by id", async () => {
            let voucher = await voucherModel.create({
                _id: new mongoose.Types.ObjectId,
                maVoucher: "code",
                phanTramGiamGia: 1,
                ghiChu: "abc"
            });

            const res = await chai
                .request(app)
                .delete("/api/devcamp-pizza365/vouchers/" + voucher._id);

            res.should.have.status(204);
            res.body.should.be.a("object");

        });
    });
})

