//B1: Import thư viện express
//import express from express
const express = require('express');
const mongoose = require("mongoose")
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");
const path = require("path");
const cors=require('cors')

//B2: Khởi tạo app express
const app = new express();
app.use(express.json())
//B3: Khai báo cổng để chạy api
const port = 8000;
// Kết nối với MongoDB:
mongoose.connect("mongodb+srv://myDB:Matkhau1@cluster0.x9asdt5.mongodb.net/Project_Pizza365")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => console.log(error))
app.use(express.static(__dirname + "/views"))
//Khai báo các api
// Router 
//CORS
app.use(cors({origin:true}))
//
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/pizza365index.html"))
})
// Sử dụng router 
app.use("/api/devcamp-pizza365/drinks", drinkRouter);
app.use("/api/devcamp-pizza365/vouchers", voucherRouter);
app.use("/api/devcamp-pizza365/users", userRouter);
app.use("/api/devcamp-pizza365/orders", orderRouter);
//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
module.exports=app